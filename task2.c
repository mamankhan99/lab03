#include<stdio.h>
#include<unistd.h>
#include<stdbool.h>
#include<stdlib.h>

float convertToFloat(char *str)
{
	bool flag = false;
	int i = 0;
	float floatNum,  num = 0,div=10;
	while (str[i] != '\0')
	{
		if((str[i] < '0' || str[i] >'9') && str[i] !='.')
		{
			printf("Invalid string");
			exit(0);
		}		
		if (flag)
		{
			floatNum = (str[i] - 48) / div;
			num = num + floatNum;
			div = div * 10;
		}
		else if (str[i] != '.')
		{
			floatNum = (str[i] - 48);
			num = num * 10 + floatNum;
		}
		else
		{
			flag = true;
		}
		i = i + 1;
	}
	return num;
}


int main(int argc,char *argv[])
{
		char *str=argv[1];
		//printf("%s",str);
		//float f=convertToFloat(str);
		printf("%f",convertToFloat(str));
		return 0;
}
